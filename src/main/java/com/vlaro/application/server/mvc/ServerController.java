package com.vlaro.application.server.mvc;


import com.vlaro.application.server.database.model.message.Message;
import com.vlaro.application.server.database.service.message.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ResponseBody
@RequestMapping("/chat")
public class ServerController {

  @Autowired
  private MessageService messageService;

  public ServerController() {
  }

  @RequestMapping("/getAllMessagesForChat")
  public List<Message> getAllMessagesForChat(@RequestParam String sender, @RequestParam String receiver) {
    return messageService.getChatMessages(sender, receiver);
  }
  // todo to be deleted ???

//  @RequestMapping("/sendMessage")
//  public void sendMessage(@RequestParam String message, String user) {
//    System.out.println(message + " : " + user);
//    Message msg = new Message();
//    msg.setReceiver(user);
//    msg.setSender(user);
//    msg.setPayload(message);
//    messageService.saveMessage(msg);
//  }

//  @RequestMapping("/getMessage")
//  public String getMessage(@RequestParam String user) {
//    String message = messageService.getMessageByReceiver(user);
//    System.out.println(message + ":" + user);
//
//    return message;
//  }
}
