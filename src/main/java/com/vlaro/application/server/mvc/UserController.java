package com.vlaro.application.server.mvc;


import com.vlaro.application.server.database.service.message.MessageService;
import com.vlaro.application.server.database.service.user.UserService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@RequestMapping("/user")
public class UserController {

  @Autowired
  private UserService userService;

  @Autowired
  private MessageService messageService;

  public UserController() {
  }

  @RequestMapping("/login")
  public long sendMessage(@RequestParam String username) {
    System.out.println(username);

    return userService.getUserId(username);
  }

  @RequestMapping("/getLastMessages")
  public JSONObject getLastMessages(@RequestParam String username) {
    JSONObject message = new JSONObject();
    switch(username) {
      case "vlad":
        message.put("roma", messageService.getChatMessages(username, "roma", 1, 1).get(0).getPayload());
        message.put("tanya", messageService.getChatMessages(username, "tanya", 1, 1).get(0).getPayload());
        break;

      case "roma":
        message.put("vlad", messageService.getChatMessages(username, "vlad", 1, 1).get(0).getPayload());
        message.put("tanya", messageService.getChatMessages(username, "tanya", 1, 1).get(0).getPayload());
        break;

      case "tanya":
        message.put("vlad", messageService.getChatMessages(username, "vlad", 1, 1).get(0).getPayload());
        message.put("roma", messageService.getChatMessages(username, "roma", 1, 1).get(0).getPayload());
        break;
    }
    return message;
  }
}
