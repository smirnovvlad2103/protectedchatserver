package com.vlaro.application.server.security;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

// todo is it really necessary
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

  @Override
  protected Class<?>[] getRootConfigClasses() {
//    return new Class[] {WebConfig.class}; // We dont need any special servlet config yet.
    return null;
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return null;
  }

  @Override
  protected String[] getServletMappings() {
    return new String[] {"/"};
  }

}

