package com.vlaro.application.server;

//    // todo: add another types of messages when needed

public enum MessageType {
    INIT("init"),
    ERROR("error"),
    APPROVED("approved"),
    CONNECTED("connected"),

    CHAT("chat"),

    UPDATE("update"),
    MESSAGE("message"),
    SECURED("secured"),

    INIT_SECURITY("init_security"),
    CLOSE_SECURITY("close_security"),
    ERROR_SECURITY("error_security");

    private String description;

    private MessageType(String description) {
        this.description = description;
    }

    public String getDescription() {return description;}
}
