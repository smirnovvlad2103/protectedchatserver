package com.vlaro.application.server.socket;

import java.util.Map.Entry;
import org.springframework.web.socket.WebSocketSession;

import java.util.concurrent.ConcurrentHashMap;

public class WebSocketSessionDB {

    private static WebSocketSessionDB instance = new WebSocketSessionDB();
    private ConcurrentHashMap<String, WebSocketSession> sessions = new ConcurrentHashMap<>();

    private WebSocketSessionDB() {
    }

    public static WebSocketSessionDB getInstance() {
        return instance;
    }

    public void registerWebSocketSession(String username, WebSocketSession webSocketSession) {
        sessions.put(username, webSocketSession);
    }

    public ConcurrentHashMap<String, WebSocketSession> getSessions() {
        return sessions;
    }

    public void removeWebSocketSession(WebSocketSession session) {

        for (Entry<String, WebSocketSession> entry : sessions.entrySet()) {
            if (entry.getValue().getId().equals(session.getId())) {
                sessions.remove(entry.getKey());
                return;
            }
        }
    }

    public WebSocketSession getSessionByName(String username) {
        return sessions.get(username);
    }

    public String getNameBySession(WebSocketSession session) {
        for (Entry<String, WebSocketSession> entry : sessions.entrySet()) {
            if (entry.getValue().getId().equals(session.getId())) {
                return entry.getKey();
            }
        }
        return null;
    }
}
