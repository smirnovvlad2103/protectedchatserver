package com.vlaro.application.server.socket;

import com.vlaro.application.server.MessageType;
import com.vlaro.application.server.chat.ServerChatManager;
import com.vlaro.application.server.database.model.message.Message;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;


@Component
public class SocketRegistrationHandler extends TextWebSocketHandler {

    private final ServerChatManager serverChatManager; // todo redo, this version doesn't work


    public SocketRegistrationHandler(ServerChatManager serverChatManager) {
        this.serverChatManager = serverChatManager;
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);
        System.out.println("New connection was established...");
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        String payload = message.getPayload();
        JSONObject payloadJSON = new JSONObject(payload);

        JSONArray messageArray = new JSONArray();
        String username = payloadJSON.getJSONObject("content")
            .getString("sender_receiver");
        if (serverChatManager.checkSign(payloadJSON, username)) {

            MessageType type = MessageType.valueOf(payloadJSON.getString("type"));
            switch (type) {
                case INIT:

                    System.out.println(
                        "HashMap size is " + WebSocketSessionDB.getInstance().getSessions().size());

                    messageArray = new JSONArray();
                    System.out.println(payloadJSON);

                    WebSocketSessionDB.getInstance()
                        .registerWebSocketSession(username, session);

                    messageArray.put(new Message("server", username, "approved").toJSONArray());

                    serverChatManager
                        .sendMessage(username, session, messageArray, MessageType.APPROVED);

                    break;
                case CHAT: // get messages from chat

                    serverChatManager.getChatMessages(payloadJSON.getJSONObject("content"));

                    break;
                case UPDATE: // send new messages from all users

                    // todo implement
                    break;
                case MESSAGE: // messages are saved on server

                    serverChatManager.saveMessage(payloadJSON.getJSONObject("content"));

                    break;
                case INIT_SECURITY:
                    serverChatManager.checkForConnection(payloadJSON);
                case SECURED: // messages mustn't be saved on server, they to be send immediately without decryption on server
                case CLOSE_SECURITY:
                    serverChatManager.sendSecuredMessage(session, payloadJSON);
                    break;
            }
        } else {

            messageArray.put(new Message("server", username, "error").toJSONArray());

            serverChatManager
                .sendMessage(username, session, messageArray, MessageType.ERROR);
            session.close(CloseStatus.PROTOCOL_ERROR);
        }
        System.out.println(payload);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        super.afterConnectionClosed(session, status);
        serverChatManager.closeOddConnection(session);
        System.out.println("Connection was closed...");
        System.out.println("HashMap size is " + WebSocketSessionDB.getInstance().getSessions().size());
    }
}
