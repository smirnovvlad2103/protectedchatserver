package com.vlaro.application.server.chat;

import com.vlaro.application.server.MessageType;
import com.vlaro.application.server.database.model.message.Message;
import com.vlaro.application.server.database.service.message.MessageService;
import com.vlaro.application.server.database.service.user.UserService;
import com.vlaro.application.server.socket.WebSocketSessionDB;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

@Component
public class ServerChatManager {

  private final UserService userService;

  private final MessageService messageService;

  private final ConcurrentHashMap<String, List<String> > secredConnectionMap;

  @Autowired
  private ServerChatManager(UserService userService, MessageService messageService) {
    this.userService = userService;
    this.messageService = messageService;
    this.secredConnectionMap = new ConcurrentHashMap<>();
  }

  public void checkForConnection(JSONObject payloadJSON) {
    Message message = Message.toMessage(payloadJSON.getJSONObject("content").getJSONArray("message").getJSONArray(0));

    if (message.getPayload().equals("success")) {
      addToMap(message.getSender(), message.getReceiver());
      addToMap(message.getReceiver(), message.getSender());
    }
  }

  public void closeOddConnection(WebSocketSession session) throws IOException {
    String offline = WebSocketSessionDB.getInstance().getNameBySession(session);
    if (secredConnectionMap != null && secredConnectionMap.contains(offline)) {
      WebSocketSessionDB.getInstance().removeWebSocketSession(session);

      for (String online : secredConnectionMap.get(offline)) {
        notifyUserOffline(WebSocketSessionDB.getInstance().getSessionByName(online), offline,
            online);
      }
    }

  }

  private void addToMap(String sender, String receiver) {
    if (secredConnectionMap.containsKey(sender)) {
      secredConnectionMap.get(sender).add(receiver);
    } else {
      List<String> list = new ArrayList<>();
      list.add(receiver);
      secredConnectionMap.put(sender, list);
    }
  }

  public void sendMessage(String receiver, JSONArray message, MessageType messageType)
      throws IOException {
    sendMessage(receiver, WebSocketSessionDB.getInstance().getSessionByName(receiver), message, messageType);
  }

  public void sendMessage(WebSocketSession session, JSONArray message, MessageType messageType)
      throws IOException {
    sendMessage(WebSocketSessionDB.getInstance().getNameBySession(session), session, message, messageType);
  }

  public void sendMessage(String receiver, WebSocketSession session, JSONArray message, MessageType messageType)
      throws IOException {
    JSONObject messageJson = new JSONObject();
    JSONObject content = new JSONObject();
    content.put("message", message);
    content.put("sender_receiver", receiver);
    messageJson.put("content", content);
    messageJson.put("type", messageType.getDescription().toUpperCase());
    signMessage(messageJson, receiver);
    if (session != null && session.isOpen()) {
      session.sendMessage(new TextMessage(messageJson.toString()));
    }
  }

  private void signMessage(JSONObject messageJson, String username) throws JSONException {
    try {

      JSONObject content = messageJson.getJSONObject("content");

      String toSign = content.getJSONArray("message") + content.getString("sender_receiver")
          + userService.getPassword(username) + messageJson.getString("type");

      toSign = toSign.replaceAll("[/\\\\]","");

      byte[] bytes = toSign.getBytes("UTF-8");


      MessageDigest sha = MessageDigest.getInstance("SHA-1");
      bytes = sha.digest(bytes);
      bytes = Arrays.copyOf(bytes, 16);

      messageJson.put("signature", new String(Base64.encodeBase64(bytes)));

    } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
      e.printStackTrace(); // todo in logs
    }
  }

  public boolean checkSign(JSONObject messageJson, String username) {
    byte[] bytes = new byte[0];

    try {

      JSONObject content = messageJson.getJSONObject("content");

      String toSign = content.getJSONArray("message") + content.getString("sender_receiver")
          + userService.getPassword(username) + messageJson.getString("type");

      toSign = toSign.replaceAll("[/\\\\]","");

      bytes = toSign.getBytes("UTF-8");

      MessageDigest sha = MessageDigest.getInstance("SHA-1");
      bytes = sha.digest(bytes);
      bytes = Arrays.copyOf(bytes, 16);
    } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
      e.printStackTrace(); // todo in logs
    }

    return messageJson.getString("signature").equals(new String(Base64.encodeBase64(bytes)));
  }

  public void saveMessage(JSONObject content) {
    JSONArray mArr = content.getJSONArray("message");

    for (int i = 0; i < mArr.length(); i++) {
      Message message = Message.toMessage(mArr.getJSONArray(i));
      messageService.saveMessage(message);
      if (WebSocketSessionDB.getInstance().getSessionByName(message.getReceiver()) != null) {
        try {
          sendMessage(message.getReceiver(), (new JSONArray()).put(message.toJSONArray()), MessageType.MESSAGE);
        } catch (IOException e) {
          e.printStackTrace(); // todo in logs
        }
      }
    }
  }

  public void sendSecuredMessage(WebSocketSession senderSession, JSONObject messageJson) throws IOException {
    JSONObject content = messageJson.getJSONObject("content");
    MessageType messageType = MessageType.valueOf(messageJson.getString("type"));
    JSONArray mArr = content.getJSONArray("message");
    String receiver = Message.toMessage(mArr.getJSONArray(0)).getReceiver();

    WebSocketSession receiverSession = WebSocketSessionDB.getInstance().getSessionByName(receiver);
    if (receiverSession != null) {
      sendMessage(receiver, receiverSession, mArr, messageType);
    } else {
      notifyUserOffline(senderSession, receiver, content.getString("sender_receiver"));
    }
  }

  private void notifyUserOffline (WebSocketSession session, String offline, String online)
      throws IOException {
    JSONArray message = new JSONArray();
    message.put(new Message(offline, online, "user is offline").toJSONArray());
    sendMessage(online, session, message, MessageType.ERROR_SECURITY);
  }

  public void getChatMessages(JSONObject content) throws IOException {
    JSONArray mArr = content.getJSONArray("message");
    Message m = Message.toMessage(mArr.getJSONArray(0));
    List<Message> messages = messageService.getChatMessages(m.getReceiver(), m.getSender());

    JSONArray mesArr = new JSONArray();
    for (Message message : messages) {
      mesArr.put(message.toJSONArray());
    }
    sendMessage(content.getString("sender_receiver"), mesArr, MessageType.CHAT);
  }
}
