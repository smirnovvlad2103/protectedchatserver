package com.vlaro.application.server.database.dao.message;

import com.vlaro.application.server.database.SQLQueries;
import com.vlaro.application.server.database.model.message.Message;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

@Repository
public class MessageDAOImpl extends JdbcDaoSupport implements MessageDAO {

    private DataSource dataSource;

    @Autowired
    public MessageDAOImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
        getJdbcTemplate().execute(SQLQueries.DROP_MESSAGES_TABLE);
        getJdbcTemplate().execute(SQLQueries.CREATE_MESSAGES_TABLE);
    }

    @Override
    public void saveMessage(Message message) {
        getJdbcTemplate().update(SQLQueries.INSERT_MESSAGE, message.getSender(), message.getReceiver(), message.getPayload());
    }

    @Override
    public Message getMessageBySender(String sender) {

        return getJdbcTemplate().queryForObject(SQLQueries.GET_MESSAGE_BY_SENDER,
            new Object[]{sender}, Message.class);
    }

    @Override
    public Message getMessageByReceiver(String receiver) {

        return getJdbcTemplate().queryForObject(SQLQueries.GET_MESSAGE_BY_RECEIVER,
            new Object[]{receiver}, Message.class);
    }

    @Override
    public List<Message> getChatMessages(String user1, String user2, int from, int to) {
        return getJdbcTemplate().queryForList(SQLQueries.GET_MESSAGE_FOR_CHAT,
            new Object[] {user1, user2, user1, user2, from, to}, Message.class);
    }

    @Override
    public List<Message> getChatMessages(String sender, String receiver) {
        List<Message> results = getJdbcTemplate().query(SQLQueries.GET_ALL_MESSAGE_FOR_CHAT, new Object[]{sender, receiver, sender, receiver},
                new RowMapper<Message>() {
                    @Override
                    public Message mapRow(ResultSet resultSet, int i) throws SQLException {
                        return new Message(resultSet.getString("sender"),
                                resultSet.getString("receiver"),
                                resultSet.getString("payload"));
                    }
                });

        return results;
    }
}
