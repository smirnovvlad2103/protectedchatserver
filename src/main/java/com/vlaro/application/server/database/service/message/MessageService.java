package com.vlaro.application.server.database.service.message;

import com.vlaro.application.server.database.model.message.Message;
import java.util.List;

public interface MessageService {
    public void saveMessage(Message message);

    public Message getMessageBySender(String sender);

    public Message getMessageByReceiver(String receiver);

    public List<Message> getChatMessages(String user1, String user2, int from, int to);

    public List<Message> getChatMessages(String sender, String receiver);

}
