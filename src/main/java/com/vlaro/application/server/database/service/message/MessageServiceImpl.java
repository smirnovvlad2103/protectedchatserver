package com.vlaro.application.server.database.service.message;

import com.vlaro.application.server.database.dao.message.MessageDAO;
import com.vlaro.application.server.database.model.message.Message;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {

    private final MessageDAO messageDAO;

    @Autowired
    public MessageServiceImpl(MessageDAO messageDAO) {
        this.messageDAO = messageDAO;
    }

    @PostConstruct
    private void initialize() {

        saveMessage(new Message("vlad", "roma", "Hello!"));
        saveMessage(new Message("vlad", "tanya", "Hi!"));
        saveMessage(new Message("tanya", "roma", "Good Morning!"));

    }

    @Override
    public void saveMessage(Message message) {
        messageDAO.saveMessage(message);
    }

    @Override
    public Message getMessageBySender(String sender) {
        return messageDAO.getMessageBySender(sender);
    }

    @Override
    public Message getMessageByReceiver(String receiver) {
        return messageDAO.getMessageByReceiver(receiver);
    }

    @Override
    public List<Message> getChatMessages(String user1, String user2, int from, int to) {
        return messageDAO.getChatMessages(user1, user2, from, to);
    }

    @Override
    public List<Message> getChatMessages(String sender, String receiver) {
        return messageDAO.getChatMessages(sender, receiver);
    }
}
