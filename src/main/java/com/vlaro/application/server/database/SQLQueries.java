package com.vlaro.application.server.database;

public class SQLQueries {
    public static final String DROP_USERS_TABLE = "drop table if exists users";

    public static final String DROP_MESSAGES_TABLE = "drop table if exists messages";

    public static final String CREATE_MESSAGES_TABLE = "create table messages " +
            "(message_id bigserial primary key," +
            " sender varchar(20) not null," +
            " receiver varchar(20) not null," +
            " payload varchar(4000)" +
            ")";

    public static final String CREATE_USERS_TABLE = "create table users " +
            "(username varchar(20) primary key not null," +
            " password varchar(60) not null," +
            " enabled boolean not null default true," +
            " role varchar(20) not null," +
            " user_id bigserial not null" +
            ")";

    public static final String INSERT_MESSAGE = "insert into messages (sender, receiver, payload) values (?, ?, ?)";

    public static final String GET_MESSAGE_BY_SENDER = "select payload from messages where sender = ? order by message desc limit 1";

    public static final String GET_MESSAGE_BY_RECEIVER = "select payload from messages where receiver = ? order by message_id desc limit 1";

    public static final String GET_MESSAGE_FOR_CHAT =
        "WITH chat_messages AS\n"
        + "(\n"
        + "    SELECT *,\n"
        + "      ROW_NUMBER() OVER (ORDER BY message_id) AS RowNumber\n"
        + "    FROM messages\n"
        + "    WHERE sender in (?,?) and receiver in (?,?)\n"
        + ")\n"
        + "SELECT sender, receiver, payload\n"
        + "FROM chat_messages\n"
        + "WHERE RowNumber BETWEEN ? AND ?";

    public static final String GET_ALL_MESSAGE_FOR_CHAT =
            "WITH chat_messages AS\n"
                    + "(\n"
                    + "    SELECT *,\n"
                    + "      ROW_NUMBER() OVER (ORDER BY message_id) AS RowNumber\n"
                    + "    FROM messages\n"
                    + "    WHERE sender in (?,?) and receiver in (?,?)\n"
                    + ")\n"
                    + "SELECT sender, receiver, payload\n"
                    + "FROM chat_messages";

    public static final String CREATE_USER = "insert into users (username, password, role) values (?, ?, ?)";

    public static final String DELETE_USER_BY_USERNAME = "delete from users where username = ?";

    public static final String GET_USER_BY_USERNAME = "select user_id from users where username = ?";

    public static final String GET_PASSWORD_BY_USERNAME = "select password from users where username = ?";
}
