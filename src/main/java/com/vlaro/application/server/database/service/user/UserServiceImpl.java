package com.vlaro.application.server.database.service.user;

import com.vlaro.application.server.database.dao.user.UserDao;
import com.vlaro.application.server.database.model.user.User;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  private final UserDao userDao;

  @Autowired
  public UserServiceImpl(UserDao userDao) {
    this.userDao = userDao;
  }


  @PostConstruct
  private void initialize() {

    createUser(new User("vlad", "vlad", "ROLE_USER"));
    createUser(new User("roma", "roma", "ROLE_USER"));
    createUser(new User("tanya", "tanya", "ROLE_USER"));
    createUser(new User("liza", "liza", "ROLE_USER"));
  }

  @Override
  public void createUser(User user) {
    userDao.createUser(user);
  }

  @Override
  public void updateUser(User user) {
    userDao.updateUser(user);
  }

  @Override
  public void deleteUser(String username) {
    userDao.deleteUser(username);
  }

  @Override
  public long getUserId(String username) {
    return userDao.getUserId(username);
  }

  @Override
  public String getPassword(String username) {
    return userDao.getPassword(username);
  }
}

