package com.vlaro.application.server.database.dao.user;

import com.vlaro.application.server.database.model.user.User;

public interface UserDao {

  void createUser(User user);

  void updateUser(User user);

  void deleteUser(String username);

  long getUserId(String username);

  String getPassword(String username);

}
