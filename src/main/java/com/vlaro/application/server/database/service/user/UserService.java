package com.vlaro.application.server.database.service.user;

import com.vlaro.application.server.database.model.user.User;

public interface UserService {

  void createUser(User user);

  void updateUser(User user);

  void deleteUser(String username);

  long getUserId(String username);

  String getPassword(String username);
}

