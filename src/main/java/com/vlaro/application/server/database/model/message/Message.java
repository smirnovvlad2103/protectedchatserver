package com.vlaro.application.server.database.model.message;

import java.io.Serializable;
import org.json.JSONArray;
import org.json.JSONException;

public class Message implements Serializable {
    private long messageId;
    private String sender;
    private String receiver;
    private String payload;

    public Message() {
    }

    public Message(String sender, String receiver, String payload) {
        this.sender = sender;
        this.receiver = receiver;
        this.payload = payload;
    }

    public Message(long messageId, String sender, String receiver, String payload) {
        this.messageId = messageId;
        this.sender = sender;
        this.receiver = receiver;
        this.payload = payload;
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {

        return toJSONArray().toString();
    }

    public JSONArray toJSONArray() {
        JSONArray message = new JSONArray();
        message.put(getMessageId());
        message.put(getSender());
        message.put(getReceiver());
        message.put(getPayload());

        return message;
    }

    public static Message toMessage(JSONArray mes) throws JSONException {
        Message message = new Message();
        message.setMessageId(mes.getLong(0));
        message.setSender(mes.getString(1));
        message.setReceiver(mes.getString(2));
        message.setPayload(mes.getString(3));

        return message;
    }
}