package com.vlaro.application.server.database.dao.user;

import com.vlaro.application.server.database.SQLQueries;
import com.vlaro.application.server.database.model.user.User;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAOImpl extends JdbcDaoSupport implements UserDao {

  private final DataSource dataSource;

  @Autowired
  public UserDAOImpl(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  @PostConstruct
  private void initialize() {
    setDataSource(dataSource);
    getJdbcTemplate().execute(SQLQueries.DROP_USERS_TABLE);
    getJdbcTemplate().execute(SQLQueries.CREATE_USERS_TABLE);
  }

  @Override
  public void createUser(User user) {
    getJdbcTemplate().update(SQLQueries.CREATE_USER, user.getUsername(), user.getPassword(), user.getRole());
  }

  @Override
  public void updateUser(User user) {

  }

  @Override
  public void deleteUser(String username) {
    try(Connection connection = getConnection()) {
      CallableStatement statement = connection.prepareCall(SQLQueries.DELETE_USER_BY_USERNAME);
      statement.setString(1, username);
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace(); //todo in logs
    }
  }

  @Override
  public long getUserId(String username) {
    return getJdbcTemplate().queryForObject(SQLQueries.GET_USER_BY_USERNAME, new Object[]{username},
        (rs, rwNumber) -> rs.getLong("user_id"));
  }

  @Override
  public String getPassword(String username) {
    return getJdbcTemplate().queryForObject(SQLQueries.GET_PASSWORD_BY_USERNAME, new Object[]{username},
        (rs, rwNumber) -> rs.getString("password"));
  }
}
